package jaredmugoya.mobileapp;

import android.support.v7.app.AppCompatActivity;
import jaredmugoya.mobileapp.R;

/**
 * Created by jared on 19/1/2019.
 */


public class transitionAnim
{
    public void fadeAnimation(AppCompatActivity a)
    {
        a.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}